import * as resttools from '../../tools/RESTtools';

//INFORMAR PARTICIPANTE PARA EXPEDIENTE
export function informarParticipante(v_idExpedienteTad:string,v_trata:string,v_codSade:string,v_cuitParticipante:string,v_perfilParticipante:string,v_interviniente:string,v_sistema:string,v_nombresParticipante:string,v_apellidosParticipante:string,v_razonSocial:string){
    let url = 'https://servicios.gcba.gob.ar/api/v1/tramites/'+v_idExpedienteTad+'/participaciones';
    let method = "POST";
    let headers = '{"Content-Type":"application/json","Accept":"application/json"}';
    let body = '{"codTipoTramite" : "'+v_trata+'","nroExpediente" : "'+v_codSade+'","cuitParticipante" : "'+v_cuitParticipante+'","idPerfil" : "'+v_perfilParticipante+'","intervinienteTad" : '+v_interviniente+',"idSistema" : "'+v_sistema+'","nombre" : "'+v_nombresParticipante+'","apellido" : "'+v_apellidosParticipante+'","razonSocial": "'+v_razonSocial+'"}'
    return resttools.apiRestConsume(url,method,headers,informarParticipante.name,body);     
}

//ELIMINAR PARTICIPANTE PARA EXPEDIENTE
export function deleteParticipante(v_idExpedienteTad:string,v_cuitParticipante:string,v_perfilParticipante:string,v_sistema:string){
    let url = 'https://servicios.gcba.gob.ar/api/v1/tramites/'+v_idExpedienteTad+'/participaciones';
    let method = "DELETE";
    let headers = '{"Content-Type":"application/json","Accept":"application/json"}';
    let body = '{"sistema":'+v_sistema+',"operador": {"cuit":'+v_cuitParticipante+',"idPerfil":'+v_perfilParticipante+'}}'
    return resttools.apiRestConsume(url,method,headers,deleteParticipante.name,body);     
}

//OBTENER PARTICIPANTES DE UN EXPEDIENTE
export function getParticipantesExpediente(v_idExpedienteTad:string){
    let url = 'https://servicios.gcba.gob.ar/api/v1/tramites/'+v_idExpedienteTad+'/participaciones';
    let method = "GET";
    let headers = '{"Content-Type":"application/json","Accept":"application/json"}';
    return resttools.apiRestConsume(url,method,headers,getParticipantesExpediente.name);     
}

//OBTENER PERFILES HABILITADOS POR TRATA DE UN EXPEDIENTE
export function getPerfilesPorTrata(v_trataTad:string){
    let url = 'https://servicios.gcba.gob.ar/api/v1/tiposTramite/'+v_trataTad+'/perfiles';
    let method = "GET";
    let headers = '{"Content-Type":"application/json","Accept":"application/json"}';
    return resttools.apiRestConsume(url,method,headers,getPerfilesPorTrata.name);     
}